import { useEffect, useRef, useState } from 'react'
import './App.css'

type coordinates = {
  x: number,
  y: number,
}

function App() {
  const [mousePos, setMousePos] = useState<coordinates>({x: 0, y: 0})
  const [eyeOffset, setEyeOffset] = useState<coordinates>({x: 0, y: 0})
  const [eyePos, setEyePos] = useState<coordinates>({x: 0, y: 0})

  const pupilRef = useRef<HTMLDivElement>(null)
  const pupilSize = 25
  const eyeCircleRadius = 20

  function onMouseMove(e: MouseEvent) {
    setMousePos({x: e.clientX, y: e.clientY})
  }

  useEffect(() => {
    addEventListener("mousemove", onMouseMove)
  }, [])

  useEffect(() => {
    let eyeCenter: coordinates = {x: (eyePos.x - eyeOffset.x), y: (eyePos.y - eyeOffset.y)}
    let mouseCircleRadius = Math.sqrt((mousePos.x - eyeCenter.x)**2 + ((mousePos.y - eyeCenter.y)**2))

    if (mouseCircleRadius < eyeCircleRadius) {
      setEyeOffset({x: mousePos.x - eyeCenter.x, y: mousePos.y - eyeCenter.y})
      return 
    }

    // Cos and sin of pupil and mouse cursor should be equal 
    // to make an illusion of eye following the mouse
    let cos = (mousePos.x - eyePos.x) / mouseCircleRadius
    let sin = ((mousePos.y - eyePos.y) / mouseCircleRadius) * -1

    let offsetEyePosX = cos * eyeCircleRadius 
    // So basically coordinates (in pixels) on the screen are 
    // flipped upside down. So we need to multiply y offset to minus 1
    let offsetEyePosY = sin * eyeCircleRadius * -1

    setEyeOffset({x: offsetEyePosX, y: offsetEyePosY})
  }, [mousePos])

  useEffect(() => {
    const divElement = pupilRef.current
    if (!divElement) {
      return
    }
    let {left, top} = pupilRef.current.getBoundingClientRect();
    
    setEyePos({x: left+pupilSize/2, y: top+pupilSize/2})
  }, [eyeOffset])

  return (
    <>
      <div className="center">
        <div className="triangle">
          <div className="eye">
            <div 
              className="pupil" 
              ref={pupilRef} 
              style={{
                top: `${eyeOffset.y}px`, 
                left: `${eyeOffset.x}px`,
                width: `${pupilSize}px`,
                height: `${pupilSize}px`,
              }}
            />
          </div>
        </div>
        <p style={{color: "#3b874f", fontSize: "1.5em"}}>
        Big brother is watching you... Always
        </p>
      </div>
    </>
  )
}

export default App
