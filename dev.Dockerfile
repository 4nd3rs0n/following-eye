FROM node:latest

WORKDIR /app

COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json

RUN npm i 

VOLUME [ "/app" ]
EXPOSE 3000

CMD ["npm", "run", "dev"]
