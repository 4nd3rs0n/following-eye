# Following eye

This is my simple project that uses React. I made it just to learn trigonometry. 

<img src="./eye.png"/>

Eye of the illuminate is always following the mouse cursor

## Run 
Will start a server on `8080` port
``` bash
# Run on the host
npm i
npm run build
npm run preview
# Run in docker
docker build -t eye-prod -f ./Dockerfile ./
docker run -p 8080:8080 eye-prod
```
## Run Dev
Will start a development server on `3000` port. Development server restarts every time you change the code. 
``` bash
# Run on the host
npm i
npm run dev
# Run in docker
docker build -t eye-dev -f ./dev.Dockerfile ./
docker run -p 3000:3000 -v ./:/app eye-dev
```